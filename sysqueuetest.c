#include <stdio.h>
#include <stdlib.h>

#if HAVE_SYS_QUEUE
# include <sys/queue.h>
#endif

#include "sysqueuetest.h"
#include "sysqueuefunc.h"


int main(void) {
	struct Element *e = new_element(1337);

	LIST_INIT(&head);
	LIST_INSERT_HEAD(&head, e, pointers);

	printlist(&head);

	return 0;
}

struct Element* new_element(int a) {
	struct Element *new = malloc(sizeof (struct Element));
	new->a = a;
	return new;
}
