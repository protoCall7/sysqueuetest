#ifndef __SYSQUEUETEST_H__
#define __SYSQUEUETEST_H__

#include "config.h"

#if HAVE_SYS_QUEUE
# include <sys/queue.h>
#endif

struct Element {
	int a;
	LIST_ENTRY(Element) pointers;
};

LIST_HEAD(mylist, Element) head;

struct Element* new_element(int a);

#endif /* __SYSQUEUETEST_H__ */
