#include "config.h"

#include <stdio.h>
#if HAVE_SYS_QUEUE
# include <sys/queue.h>
#endif

#include "sysqueuefunc.h"
#include "sysqueuetest.h"

void printlist(struct mylist *head) {
	struct Element *e = new_element(0);

	LIST_FOREACH(e, head, pointers)
		printf("%d\n", e->a);
}
