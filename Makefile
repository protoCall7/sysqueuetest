include Makefile.configure

all: sysqueuetest

compats.o: compats.c
	${CC} ${CFLAGS} -o $@ -c $<

sysqueuefunc.o: sysqueuefunc.c sysqueuefunc.h sysqueuetest.h
	${CC} ${CFLAGS} -o $@ -c sysqueuefunc.c

sysqueuetest: sysqueuetest.c sysqueuefunc.o compats.o
	${CC} ${CFLAGS} -o $@ sysqueuefunc.o compats.o sysqueuetest.c

clean:
	rm -rf sysqueuetest *.o *.dSYM

.PHONY: all
